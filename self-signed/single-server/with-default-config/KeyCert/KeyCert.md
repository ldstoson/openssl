## 기본 목표 
1. <code>toson.asianaidt.com</code> 단일 서버에 대한 개인키와 자체서명 인증서 생성
1. private key encryption 없음


## 기본 절차

1. 개인키와 Cert을 KeyCert.key KeyCert.crt 동시 생성


## 문제점

* 버전 1로 생성된다
* 기본 설정파일에 따라 키와 인증서는 생성되지만 SSL/TLS 인증서로 적합한 속성이 없는 것 같다


## (1단계) RSA 개인키와 인증서를 한꺼번에 생성(CSR 생략)

```
...>openssl req -newkey rsa:2048 -keyform PEM -keyout KeyCert.key -noenc -x509 -days 365 -out KeyCert.crt -outform PEM -sha256 -section req -subj /C=KR/CN=toson.asianaidt.com -verbose

Using configuration from C:\OpenSSL-Win64\bin\openssl.cfg
Generating rsa key with 2048 bits
.+...+........+...+......+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*..+......+.........+......+.+............+..+.+........+.......+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*.+.....+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
......+...+......+......+.....+...............+...+.......+...........+...+.+...........+...+............+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*........+...+..+...+....+............+.....+.........+.......+..+...+.........+....+......+.........+.....+......+.+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*......+.................+...+.............+..+.+..+....+.....+.+............+........+.........+...+.+.........+..........................+....+......+.....+.+.........+......+.....+.+..+.....................+.......+..+...+....+.......................+.+..+...+.+...+..+...+......+......+....+.........+...+.....+...+.+...+...+............+............+...........+....+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
Writing private key to 'KeyCert.key'
-----
```


## 인증서 검증
이렇게 확인하면 신뢰할 수 없는 인증 기관이 서명한 인증서여서 검증 오류가 발생한다.
그러나 몇 가지 추가적인 조치를 취하면 SSL/TLS 인증서로 사용할 수 있다. 
```
...>openssl verify KeyCert.crt
C = KR, CN = toson.asianaidt.com
error 18 at 0 depth lookup: self-signed certificate
error KeyCsrCert.crt: verification failed

...>openssl x509 -in KeyCert.crt -verify -noout

```


## 인증서 내용 조회
```
...>openssl x509 -in KeyCert.crt -text -noout

Certificate:
    Data:
        Version: 3 (0x2)
        Serial Number:
            71:7d:b4:01:bc:60:01:9a:27:69:1d:1f:2e:d0:6a:98:5a:db:46:6c
        Signature Algorithm: sha256WithRSAEncryption
        Issuer: C = KR, CN = toson.asianaidt.com
        Validity
            Not Before: Jan 18 09:25:35 2023 GMT
            Not After : Jan 18 09:25:35 2024 GMT
        Subject: C = KR, CN = toson.asianaidt.com
        Subject Public Key Info:
            Public Key Algorithm: rsaEncryption
                Public-Key: (2048 bit)
                Modulus:
                    00:cc:6a:45:ab:a9:84:0c:5c:a7:be:6d:bc:cb:37:
                    64:22:06:6e:1b:83:c9:fa:f0:4f:10:4b:66:93:af:
                    e6:64:68:7a:20:66:78:39:49:77:66:5a:fd:c4:09:
                    19:9e:04:90:be:99:a4:b0:c8:7f:ba:dc:66:ed:eb:
                    eb:13:33:da:07:a1:c9:71:2b:1a:1b:c8:71:dd:3e:
                    4b:d7:db:bb:be:63:3a:b1:c0:ed:7d:2a:a8:2a:bf:
                    59:fa:79:6b:24:f9:30:6c:fd:ba:c2:17:74:42:66:
                    eb:dc:b2:38:3b:00:c6:00:0f:6d:38:f5:e0:40:52:
                    e3:4a:d5:77:00:6b:d4:bb:88:fb:53:b6:2c:05:dd:
                    e6:f3:86:0a:d5:8d:82:2f:69:ef:76:7d:48:29:2f:
                    a2:c5:3d:2d:6a:1f:8f:2d:6f:76:87:a9:3f:55:43:
                    9c:03:99:3d:4f:b7:b9:cd:86:23:2a:d9:b3:b8:49:
                    f2:56:59:f1:e3:d4:7d:70:c3:e8:57:99:ff:77:a6:
                    1e:96:69:cb:f0:c9:5b:bf:77:c6:34:ed:ab:48:3c:
                    45:1c:87:67:0c:e9:46:b7:04:fb:74:0c:78:59:ad:
                    86:23:a8:c3:ec:5e:0b:9e:0c:5d:34:d7:80:07:3b:
                    c9:4c:da:3f:d5:40:7e:c1:ba:86:ca:ee:54:e5:54:
                    24:c7
                Exponent: 65537 (0x10001)
        X509v3 extensions:
            X509v3 Subject Key Identifier:
                8C:61:17:CD:BE:09:46:AE:E9:F8:97:B0:79:28:47:3B:D0:EE:6A:BB
            X509v3 Authority Key Identifier:
                8C:61:17:CD:BE:09:46:AE:E9:F8:97:B0:79:28:47:3B:D0:EE:6A:BB
            X509v3 Basic Constraints: critical
                CA:TRUE
    Signature Algorithm: sha256WithRSAEncryption
    Signature Value:
        70:31:b0:01:af:5e:54:06:02:c2:a7:3f:22:56:da:fc:11:ae:
        cd:f1:cb:30:58:7d:83:f5:1c:f2:67:06:29:c2:2c:05:d0:58:
        1f:08:47:2b:9e:5c:be:c8:48:2e:a2:83:2c:ce:3b:1d:8a:cf:
        04:f2:f8:02:c5:c9:b6:b8:28:60:80:74:d6:f6:98:26:d7:bb:
        80:a4:5c:61:2b:17:45:55:e0:1f:ce:21:e9:8b:b8:0e:a0:81:
        84:27:6f:4c:5c:f3:b3:73:10:b3:7d:cd:dc:83:65:b5:ea:f5:
        15:43:9e:e9:59:1c:f2:a9:66:06:da:f8:99:91:11:b7:54:34:
        af:48:da:c1:c5:b1:54:2e:f3:af:16:d6:c0:d5:47:fe:9e:40:
        b8:cf:45:c0:7f:c8:12:dc:aa:db:94:71:e7:56:4d:1b:e5:00:
        cd:c1:99:c2:0c:89:ae:54:d1:70:40:93:ea:92:8f:69:11:ae:
        3c:a3:b3:13:1f:90:fc:38:7f:c2:96:48:c2:27:61:f4:4c:55:
        5a:cf:07:a8:b4:92:02:f5:81:93:59:b4:d8:6d:7b:86:bd:ea:
        69:fb:42:8f:14:d5:b0:da:4d:a8:6d:5c:0b:57:46:24:28:f4:
        46:7f:94:79:fe:da:15:b6:fb:70:81:c6:a3:48:e0:52:b6:ad:
        45:d8:16:14
```

## PEM 포맷 인증서를 DER 포맷으로 변환
이렇게 포맷이 변환된 인증서는 저장형식은 다르지만 내용은 동일하다
```
...>openssl x509 -in KeyCert.crt -outform der -out KeyCert.der

```

