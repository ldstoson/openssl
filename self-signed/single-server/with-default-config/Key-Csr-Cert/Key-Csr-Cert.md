## 목표 
<code>toson.asianaidt.com</code> 서버에 대한 자체서명 인증서 생성

## 기본 절차

1. 비밀키를 myserver.key 라는 파일에 생성
1. CSR을 myserver.csr 라는 파일에 생성
1. 인증서를 myserver.crt 라는 파일에 생성

## 특징 요약

* OpenSSL 3.0.7 버전을 기준으로 한다
* OpenSSL 3.0.7 설치시 제공된 기본 설정파일을 사용한다.
* 버전 1로 생성된다
* 기본 설정파일에 따라 키와 인증서는 생성되지만 SSL/TLS 인증서로 적합한 속성이 없는 것 같다


## 참조링크

* 참조링크1 : <https://www.baeldung.com/openssl-self-signed-cert>
* 참조링크2 : <https://www.ibm.com/docs/en/hpvs/1.2.x?topic=servers-creating-ca-signed-certificates-monitoring-infrastructure>

## 설치된 openssl 디렉토리 확인
설치된 환경에 따라 다르게 표시될 것이다.
```
C:\Users\USER>where openssl
C:\OpenSSL-Win64\bin\openssl.exe
```


## 설치된 openssl version 확인
설치한 버전에 따라 다르게 표시될 것이다.
```
C:\Users\USER>openssl version
OpenSSL 3.0.7 1 Nov 2022 (Library: OpenSSL 3.0.7 1 Nov 2022)
```

## openssl 관련 환경변수

```
C:\Users\USER>set
OPENSSL_CONF=C:\OpenSSL-Win64\bin\openssl.cfg
Path=...;C:\OpenSSL-Win64\bin;...
```

## 디폴트 설정파일
사용자가 명령 파라미터로 특정 설정파일을 제공하지 않으면 
OPENSSL_CONF 환경변수에 지정된 기본 설정파일이 사용된다.

```C:\OpenSSL-Win64\bin\openssl.cfg```

## 인증서를 저장할 디렉토리로 이동
이후 디렉토리는 ... 으로 생략함
```
C:\Users\USER>C:\Projects\gitlab.com\ldstoson\openssl\self-signed\with-default-config
```

## (1단계) RSA 4096 비트 비밀키 생성
```
...>openssl genrsa -out myserver.key 4096

...>dir myserver.key
 C 드라이브의 볼륨: Window10
 볼륨 일련 번호: 58B8-C788

 ... 디렉터리

2023-01-13(금)  오후 04:53             3,324 myserver.key
               1개 파일               3,324 바이트
               0개 디렉터리  41,034,788,864 바이트 남음
```


## (2단계) CSR 생성

```
...>openssl req -new -key myserver.key -keyform PEM --out myserver.csr -outform PEM -sha256 -verbose
C:\Projects\gitlab.com\ldstoson\openssl>openssl req -verbose -new -key myserver.key -out myserver.csr -sha256
Using configuration from C:\OpenSSL-Win64\bin\openssl.cfg
You are about to be asked to enter information that will be incorporated
into your certificate request.
What you are about to enter is what is called a Distinguished Name or a DN.
There are quite a few fields but you can leave some blank
For some fields there will be a default value,
If you enter '.', the field will be left blank.
-----
Country Name (2 letter code) [AU]:KR
State or Province Name (full name) [Some-State]:
Locality Name (eg, city) []:
Organization Name (eg, company) [Internet Widgits Pty Ltd]:Asiana IDT Inc.
Organizational Unit Name (eg, section) []:TI Team
Common Name (e.g. server FQDN or YOUR name) []:toson.asianaidt.com
Email Address []:toson@asianaidt.com

Please enter the following 'extra' attributes
to be sent with your certificate request
A challenge password []:
An optional company name []:

...>dir myserver.*
 C 드라이브의 볼륨: Window10
 볼륨 일련 번호: 58B8-C788

 ... 디렉터리

2023-01-13(금)  오후 05:19             1,780 myserver.csr
2023-01-13(금)  오후 04:53             3,324 myserver.key
               2개 파일               5,104 바이트
               0개 디렉터리  41,024,573,440 바이트 남음
```

## (3단계)비밀키와 CSR을 이용하여 자체 서명한 인증서 생성
```
...> openssl x509 -signkey myserver.key -in myserver.csr -req -days 365 -out myserver.crt
csr -req -days 365 -out myserver.crt
Certificate request self-signature ok
subject=C = KR, ST = Some-State, O = Asiana IDT Inc., OU = TI Team, CN = toson.asianaidt.com, emailAddress = toson@asianaidt.com

...>dir myserver.*
 C 드라이브의 볼륨: Window10
 볼륨 일련 번호: 58B8-C788

 C:\Projects\gitlab.com\ldstoson\openssl\self-signed\with-default-config 디렉터리

2023-01-13(금)  오후 05:47             2,058 myserver.crt
2023-01-13(금)  오후 05:19             1,780 myserver.csr
2023-01-13(금)  오후 04:53             3,324 myserver.key
2023-01-13(금)  오후 05:26            68,764 myserver.png
               4개 파일              75,926 바이트
               0개 디렉터리  41,011,515,392 바이트 남음
```
myserver.png 파일은 myserver.csr 파일의 내용을 <https://www.sslcert.co.kr/tools/certificate-csr-decoder> 에서 디코드하여 확인한 내용을 캡쳐한 것임

이렇게 생성된 인증서는 그 자체로 루트 인증기관 역할을 겸한다.

## 인증서 확인1
```
...> openssl x509 -text -noout -in myserver.crt
Certificate:
    Data:
        Version: 1 (0x0)
        Serial Number:
            0a:62:72:2a:05:84:c6:fa:05:6c:fd:87:3d:4e:40:35:21:d9:81:f8
        Signature Algorithm: sha256WithRSAEncryption
        Issuer: C = KR, ST = Some-State, O = Asiana IDT Inc., OU = TI Team, CN = toson.asianaidt.com, emailAddress = toson@asianaidt.com
        Validity
            Not Before: Jan 13 08:47:44 2023 GMT
            Not After : Jan 13 08:47:44 2024 GMT
        Subject: C = KR, ST = Some-State, O = Asiana IDT Inc., OU = TI Team, CN = toson.asianaidt.com, emailAddress = toson@asianaidt.com
        Subject Public Key Info:
            Public Key Algorithm: rsaEncryption
                Public-Key: (4096 bit)
                Modulus:
                    00:b7:7e:75:7d:08:9c:31:12:b6:8f:d9:d6:c6:3e:
                    e6:80:a2:e3:a4:6b:21:c9:ba:82:3f:4f:38:d9:c2:
                    21:45:19:b6:6a:a1:e2:90:25:6d:ce:24:1d:e4:d9:
                    09:48:53:97:7d:fe:c6:59:63:30:d3:7e:ab:6b:6c:
                    ae:8e:17:8e:cf:39:df:d8:b2:4d:11:33:bf:52:c0:
                    f9:d3:18:41:2b:13:e0:fa:b5:8c:49:87:b7:2f:ba:
                    70:29:10:00:2c:27:54:3f:92:b3:79:e7:b6:28:c7:
                    2f:7b:05:47:c2:66:dd:86:88:2b:1b:9c:89:31:ef:
                    5a:1e:b0:2d:dd:1b:f2:f1:14:5a:8c:d9:66:7b:f6:
                    26:f8:5d:42:3f:2c:df:63:a1:1a:c0:23:05:6b:62:
                    44:e6:c0:9c:33:6d:3a:ab:6b:e2:47:a0:04:b7:a2:
                    8d:00:f6:6a:f5:77:cc:20:43:a2:d9:74:6c:d8:7d:
                    ec:77:75:8d:6e:12:82:d7:69:06:18:87:c7:21:87:
                    56:eb:a9:15:3c:bd:6b:4b:46:24:bf:eb:90:0f:00:
                    f8:ce:4d:9c:f9:23:f4:ac:a5:a0:d0:f1:46:92:9a:
                    e9:70:8a:8b:7a:d2:85:eb:31:19:9a:ef:f9:02:2a:
                    5c:eb:0d:ac:ac:fa:50:49:ca:35:4d:0b:91:74:f9:
                    28:8e:8f:fd:f1:74:07:48:8b:69:be:a4:40:f8:5c:
                    fd:b5:c1:16:9b:7e:70:00:b6:8f:94:7e:e0:e2:3f:
                    34:c9:15:e8:d0:d5:b7:40:43:fa:17:4f:7c:3a:27:
                    c4:f3:55:f8:d6:75:83:3d:7e:03:1d:34:d4:de:d7:
                    d9:55:c7:e1:4a:9c:f4:57:d1:7a:f1:1f:af:82:32:
                    8b:b8:40:92:8d:a9:56:6a:2f:a2:cb:db:b9:41:f1:
                    10:f3:90:ee:8c:a9:b6:2e:79:d3:23:c8:5e:0d:7e:
                    4d:65:0d:13:df:2a:71:93:da:09:df:89:64:7a:bf:
                    8a:a2:b6:e7:57:c3:5c:19:45:14:42:45:fc:ce:85:
                    8f:02:fa:9d:83:13:00:5c:dd:da:7d:21:a3:e1:bb:
                    0f:d3:36:52:1d:e4:8b:2d:2d:e9:77:38:70:16:52:
                    81:7c:de:75:fe:78:db:85:dd:27:e5:f2:88:02:e5:
                    14:1c:04:6e:47:3e:49:2e:77:3b:fd:bb:74:56:5f:
                    b6:8d:d6:09:58:2e:cb:18:9d:41:28:47:bc:b0:82:
                    7d:e0:8b:47:cb:3e:d7:ae:cb:82:af:90:39:7b:29:
                    38:b8:50:42:b7:0a:71:9e:6d:9b:01:76:44:1c:ae:
                    18:fd:5b:c9:47:a0:bd:6c:da:0f:0e:3b:8a:fb:06:
                    37:42:7b
                Exponent: 65537 (0x10001)
    Signature Algorithm: sha256WithRSAEncryption
    Signature Value:
        a4:03:0c:eb:4d:f9:f4:a7:49:c1:73:84:64:da:c4:84:44:0b:
        99:d6:00:58:99:3d:89:a1:32:99:f8:aa:d0:95:8c:1e:2a:90:
        bb:4d:63:79:d0:c5:f0:20:de:f5:27:bc:ac:3f:f5:30:db:7a:
        bf:2e:a4:e2:34:68:09:aa:55:d8:31:c6:a1:37:be:48:de:2a:
        fb:20:5e:83:7f:3a:7c:c7:c1:07:31:3c:c7:24:a5:60:ab:e7:
        9c:ea:c1:60:fe:64:52:a5:da:ec:75:f6:ce:22:12:9b:89:2a:
        18:65:44:21:fb:1f:da:a6:3c:cd:d8:ad:64:30:ae:ba:2f:4e:
        05:20:5c:0e:3f:85:c7:45:92:79:db:ef:49:6e:ed:f9:e3:de:
        b7:53:69:02:e3:69:07:98:42:76:37:3c:62:a8:a7:0f:7a:f1:
        d4:58:8d:12:73:cf:0c:fc:4e:5a:0b:eb:7f:ec:22:59:0f:6a:
        8a:e5:d8:fb:8f:85:4f:a4:22:27:3c:22:77:13:7c:17:b1:12:
        32:05:bb:43:7d:16:8c:d2:f5:58:11:71:81:ee:c2:8d:e5:3f:
        a7:ac:f9:a4:a8:77:91:0f:7f:29:1b:af:37:e4:02:94:ae:05:
        b6:96:98:c8:dc:74:71:49:ce:a8:ad:c5:e6:6c:27:5e:1d:0c:
        bc:be:a7:d0:0f:4c:92:f7:21:33:32:6f:39:38:ae:d7:de:d2:
        60:5d:fb:4b:5d:97:b3:c2:8c:ef:68:b6:2b:e1:6c:18:82:bb:
        7a:ab:0f:b5:0f:86:7e:75:82:bb:4e:3e:d1:80:d6:6c:29:55:
        2b:e9:91:c8:dc:ef:b1:f8:c4:0d:14:8e:88:aa:8e:a0:ce:32:
        59:df:99:c9:54:a6:fc:53:65:d6:3c:3f:5e:37:fb:3e:04:07:
        3c:bc:68:88:b7:ee:0a:7d:4c:9c:d8:d3:a3:44:78:fa:23:aa:
        72:95:8d:15:8e:24:96:cf:98:45:95:f6:94:0d:b6:ba:bd:06:
        ca:a8:ef:89:f6:63:68:df:8c:99:3a:36:cd:c7:4e:ad:06:45:
        df:89:e0:9f:7c:5e:55:09:a2:5a:4e:d1:d2:e6:c3:37:3f:d0:
        39:39:cc:a4:e6:4a:9e:cc:81:43:fa:f3:03:ea:7e:4d:bd:be:
        2d:ba:23:5b:ab:cd:15:0c:fc:b2:92:fd:40:e3:e0:70:f0:86:
        9f:a3:93:fd:9e:e4:ba:f2:31:79:04:a7:6f:97:79:ff:8f:7f:
        41:8f:95:82:0d:23:37:31:39:3a:21:42:6a:26:ee:07:a2:d8:
        e2:ba:1b:fc:5e:01:ca:16:1a:86:87:dd:29:e2:3f:47:0f:8a:
        12:5c:d8:e2:09:be:30:b7
```

## 인증서 검증
이렇게 확인하면 신뢰할 수 없는 인증 기관이 서명한 인증서여서 검증 오류가 발생한다.
그러나 몇 가지 추가적인 조치를 취하면 SSL/TLS 인증서로 사용할 수 있다. 
```
...>openssl verify myserver.crt
C = KR, ST = Some-State, O = Asiana IDT Inc., OU = TI Team, CN = toson.asianaidt.com, emailAddress = toson@asianaidt.com
error 18 at 0 depth lookup: self-signed certificate
error myserver.crt: verification failed
```


## PEM 포맷 인증서를 DER 포맷으로 변환
이렇게 포맷이 변환된 인증서는 저장형식은 다르지만 내용은 동일하다
```
...>openssl x509 -in myserver.crt -outform der -out myserver.der
```

## 현재까지 생성된 파일 확인
이 중에서 웹 사이트에 필요한 파일은 myserver.key, myserver.crt/myserver.der 뿐이다.
```
...>dir
 C 드라이브의 볼륨: Window10
 볼륨 일련 번호: 58B8-C788

 C:\Projects\gitlab.com\ldstoson\openssl\self-signed\with-default-config 디렉터리

2023-01-13(금)  오후 06:02    <DIR>          .
2023-01-13(금)  오후 06:02    <DIR>          ..
2023-01-13(금)  오후 06:02            95,722 CSR디코드결과.png
2023-01-13(금)  오후 05:47             2,058 myserver.crt
2023-01-13(금)  오후 05:19             1,780 myserver.csr
2023-01-13(금)  오후 05:55             1,453 myserver.der
2023-01-13(금)  오후 04:53             3,324 myserver.key
2023-01-13(금)  오후 05:57            16,925 myserver.png
2023-01-13(금)  오후 05:59            11,630 인증서_인증경로.png
2023-01-13(금)  오후 05:58            16,925 인증서_일반.png
2023-01-13(금)  오후 05:58            16,573 인증서_자세히1.png
2023-01-13(금)  오후 06:00            21,530 인증서_자세히2.png
              10개 파일             187,920 바이트
               2개 디렉터리  40,980,733,952 바이트 남음
```
